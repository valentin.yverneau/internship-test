﻿using UnityEngine;
using System;

public class TransformManipulationBox : MonoBehaviour {
    [SerializeField] private Transform encompassedTransform;
 
    public void Start()
    {
        EncompassedObject();
    }

    public void EncompassedObject()
    {
        Bounds boundsObj = getBounds();

        SetScaleHandler(boundsObj);
        SetTubes(boundsObj);
        SetRotHandler(boundsObj);
        SetMoveHandler(boundsObj);
    }

    private Transform getScaleHandlers()
    {
        return encompassedTransform.parent.GetChild(0);
    }

    private Transform getTubes()
    {
        return encompassedTransform.parent.GetChild(2);
    }

    private Transform getRotHandler()
    {
        return encompassedTransform.parent.GetChild(1);
    }

    private Transform getMoveHandler()
    {
        return encompassedTransform.parent.GetChild(3);
    }

    private Bounds getBounds()
    {
        Renderer tmp = encompassedTransform.GetComponent<Renderer>();
        if (tmp == null)
        {
          // Code from a Google Search
            MeshFilter[] meshFilters = encompassedTransform.GetComponentsInChildren<MeshFilter>();
            CombineInstance[] combine = new CombineInstance[meshFilters.Length];
            int i = 0;
            while (i < meshFilters.Length)
            {
                combine[i].mesh = meshFilters[i].sharedMesh;
                combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
                i++;
            }
            Mesh res = new Mesh();
            res.CombineMeshes(combine);
          //End of Google Search
            return res.bounds;
        }
        else
            return tmp.bounds;
    }

    private void SetScaleHandler(Bounds newBounds)
    {
        Transform t = getScaleHandlers();
        t.localScale = newBounds.size;
    }

    private void SetRotHandler(Bounds newBounds)
    {
        Transform t = getRotHandler();
        t.localScale = newBounds.size;
    }

    private void SetTubes(Bounds newBounds)
    {
        Transform t = getTubes();
        t.localScale = newBounds.size;
    }

    private void SetMoveHandler(Bounds newBounds)
    {
        Transform t = getMoveHandler();
        t.localScale = newBounds.size;
    }


}
